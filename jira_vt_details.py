from jira import JIRA
import creds as CREDS
import re
import simplejson
import urllib
import urllib2
import sys

APIKEY = CREDS.APIKEY
servername = CREDS.server
options = {'server': servername}
user = CREDS.user
password = CREDS.password
jira = JIRA(options, basic_auth=(user, password))

HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'


def main():
	# keep track of how many queries we've made so limit API exhaustion
	total_checked = 0 
	list_of_issues = jira.search_issues('project = INC AND created > startofday()', maxResults=100, fields = "reporer,summary,description,comment,created,reporter,updated,status,assignee")
	print	
	print HEADER + "Total Issues: " + str(list_of_issues.total) + ENDC
	
	for issue in list_of_issues:
		print
		print HEADER + str(issue.key) + " - " + str(len(issue.fields.comment.comments)) + " comments." + ENDC 
		# Check if we've made too many requests already and quit if so
		if total_checked == 4:
			sys.exit(FAIL + '[*] We hit our API limit, wait a minute and try again' + ENDC)
		
		# Check if we should skip this issue because it already has comments from our bot	
		skip = already_done(issue)
		
		# If we should skip this, break to the next issue
		if skip:
			print "[*] Skip is True.  Moving on to the next issue."
			continue
		
		# If we're not skipping, check out the description and find a virustotal link
		if skip == False:
			print "[*] Checking description for virustotal links."
			mydesc = issue.fields.description
			finddesc = re.search("https://[w\.]*virustotal.com/en/file/([A-Za-z0-9]+)/analysis", mydesc)
			
			# If we found something, parse, get results, increase total_checked, put details, set skip so we move on
			if finddesc:
				print "[*] Found virustotal link in description: " + str(finddesc.group(1))
				positives, total, scan_date = get_VT_results(finddesc.group(1), APIKEY)
				total_checked += 1
				if total == 0:
					comment_to_append = "[*] VirusTotal Link: " + str(finddesc.group(0)) + "\n" + "[*] VirusTotal Results: Not Found"
				else:
					comment_to_append = "[*] VirusTotal Link: " + str(finddesc.group(0)) + "\n" + "[*] VirusTotal Results: " + str(positives) + "/" + str(total) + " (" + str(str(scan_date)) + ")"
				
				print comment_to_append
				#jira.add_comment(issue, comment_to_append)
				# set skip so we don't parse comments after finding a hit in description
				skip = True	
			else:
				print "[*] NO VT Link in description."

			
		# if we have not found any existing comments, parse comments for VT links
			for comment in issue.fields.comment.comments:
				if skip == False:
					print "[*] Checking comments for virustotal links."
					mycomm = comment.raw['body']
					findit = re.search("https://[w\.]*virustotal.com/en/file/([A-Za-z0-9]+)/analysis", mycomm)
					if findit:
						print "[" + str(issue.key) + "] Found virustotal link in comments: " + str(findit.group(1))
						positives, total, scan_date = get_VT_results(findit.group(1), APIKEY)
						total_checked += 1
						if total == 0:
							comment_to_append = "[*] VirusTotal Link: " + str(findit.group(0)) + "\n" + "[*] VirusTotal Results: Not Found"
						else:
							comment_to_append = "[*] VirusTotal Link: " + str(findit.group(0)) + "\n" + "[*] VirusTotal Results: " + str(positives) + "/" + str(total) + " (" + str(str(scan_date)) + ")"
						print comment_to_append
						#jira.add_comment(issue, comment_to_append)
						# set skip so we don't parse comments after finding a hit in comments
						skip = True
					else:
						print "[*] NO VT Link in comments." 

				
		
def already_done(issue):
	for comment in issue.fields.comment.comments:
		mycomm = comment.raw['body']
		already_done = re.search("VirusTotal Results", mycomm)
		if already_done:
			return True
			break
	return False
				


def get_VT_results(resource, APIKEY):
    URL = "https://www.virustotal.com/vtapi/v2/file/report"
    parameters = {"resource" : resource,
                "apikey" : APIKEY}
    
    data = urllib.urlencode(parameters)
    req = urllib2.Request(URL, data)
    response = urllib2.urlopen(req)
    json = response.read()
    parsed_json = simplejson.loads(json)
    if parsed_json['response_code'] == 1:
        return parsed_json['positives'], parsed_json['total'], parsed_json['scan_date']
    else:
        return 0, 0, 0 
		
main()



# ===== old=====

	# https://www.virustotal.com/en/file/0930B49DD929B6E3AF52A77771E290736D6D98068549C55A2ED7C9C735833348/analysis/
	  #if re.search('VT', mycomm):
	  #  print issue.key + " - " + issue.fields.created + " - " + issue.fields.reporter.name + " - ",
  #  print issue.fields.summary + " - " + issue.fields.status.name + " - " + getattr(issue.fields.assignee, "name", "")

#Add a comment stating that we're reassigning it
  # Reassign the ticket to the person who reported it


#  #jira.add_comment(issue, '*This ticket is more than 10 days old and unassigned. Reassigning to Reporter.*')
#  try:
#    jira.assign_issue(issue, issue.fields.reporter.name)
#  except:
#    jira.assign_issue(issue, 'e19138')

